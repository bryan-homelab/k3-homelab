vms = {
  master01 = {
    name   = "k3-master-dev-01"
    ip     = "10.10.10.31"
    cpu    = 1
    memory = 1000
    node   = "pve-02"
  }
  worker01 = {
    name   = "k3-worker-dev-01"
    ip     = "10.10.10.33"
    cpu    = 1
    memory = 1000
    node   = "pve-02"
  }

}
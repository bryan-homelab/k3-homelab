variable "pve_password" {
  type        = string
  sensitive   = true
  description = "Password for the user for proxmox"
}

variable "pihole_api_token" {
  type        = string
  description = "API token for your pihole instance"
  sensitive   = true
}

variable "pve_url" {
  type        = string
  description = "URL to the api of your promxox instance"
  default     = "https://pve-01.homelab.com:8006/api2/json"
}

variable "pihole_url" {
  type        = string
  description = "URL to your pihole instance"
  default     = "http://pi.hole/"
}

variable "vms" {
  type        = map(any)
  description = "Map of VMs to build, must include name and IP"
}
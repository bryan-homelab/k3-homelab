vms = {
  master01 = {
    name   = "k3-master-prod-01"
    ip     = "10.10.10.21"
    cpu    = 2
    memory = 2000
    node   = "pve-02"
  }
  worker01 = {
    name   = "k3-worker-prod-01"
    ip     = "10.10.10.23"
    cpu    = 2
    memory = 2000
    node   = "pve-02"
  }
}